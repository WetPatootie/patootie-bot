const Discord = require('discord.js');
const client = new Discord.Client();
const settings = require('./oof.json');
const info = require('./package.json');

client.on('ready', () => {
	console.log('Logged in as $(client.user.tag!)');
});

client.on('message', msg => {
	var command = msg.content.split (" ");
	var params = command.slice(1, command.length).join(" ");

	switch(command[0].toLowerCase()){
		case ".ping":
		ping(msg)
		break;

		case ".version":
		version(msg);
		break;

		case ".avatar":
		avatar(msg);
		break;

		case ".echo":
		echo(msg, params);
		break;
	}
});

function ping(msg) {
	msg.reply("Pong!");
}

function version(msg) {
	msg.reply(info.Version);
}

function avatar(msg){
	msg.reply(msg.author.avatarURL);
}

function echo(msg, params){
	var echoMsg = msg.reply(params);
	msg.channel.send(echoMsg);
}

client.login (settings.token);
